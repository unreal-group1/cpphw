﻿#include <iostream>
#include "Fraction.h"

void UserTryCreateFraction()
{
    int FirstUserInput = 0;
    int SecondUserInput = 0;

    std::cout << "Input Numerator\n";
    std::cin >> FirstUserInput;
    std::cout << "Input Denominator\n";
    std::cin >> SecondUserInput;

    Fraction* fraction = nullptr;
    try
    {
        fraction = new Fraction(
            FirstUserInput,
            SecondUserInput
        );
    }
    catch (std::exception)
    {
        std::cerr << "Incorrect data entered. \nPlease try again\n\n";
        UserTryCreateFraction();
    }
}

int main()
{
    UserTryCreateFraction();
}
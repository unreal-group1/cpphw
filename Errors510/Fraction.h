#pragma once
#include <stdexcept>

class Fraction
{
	public:
	Fraction();
	Fraction(int Numerator, int Denominator);

	private:
	int numerator;
	int denominator;
};


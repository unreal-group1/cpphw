#include "Fraction.h"

Fraction::Fraction()
{
	numerator = 1;
	denominator = 1;
}

Fraction::Fraction(int Numerator, int Denominator) 
{
	if (Denominator == 0)
	{
		throw std::runtime_error(std::string("Denominator cannot be equal to zero "));
	}
	else
	{
		numerator = Numerator;
		denominator = Denominator;
	}
}
